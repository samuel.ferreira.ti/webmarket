const express = require('express');
const authMiddleware = require('../middlewares/auth')
const Item = require('../models/item');
const router = express.Router();

router.use(authMiddleware);

router.get('/', async (req, res) => {
    try {
        const items = await Item.find().populate(['usuario']);

        return res.send({ items });
    } catch (err) {
        return res.status(400).send({ error: 'Erro ao carregar lista de itens' });
    }
});

router.get('/:itemId', async (req, res) => {
    try {
        const item = await Item.findById(req.params.itemId).populate(['usuario']);

        return res.send({ item });
    } catch (err) {
        return res.status(400).send({ error: 'Erro ao buscar item' });
    }
});

router.post('/', async (req, res) => {
    try {
        const { nome, descricao, valor, valorAnterior, imagemURI } = req.body;

        const item = await Item.create({ nome, descricao, valor, valorAnterior, imagemURI, usuario: req.userId });

        await item.save();

        return res.send({ item });
    } catch (err) {
        return res.status(400).send({ error: 'Erro ao criar um item' });
    }
});

router.put('/:itemId', async (req, res) => {
    try {
        const { nome, descricao, valor, valorAnterior, imagemURI } = req.body;

        const item = await Item.findByIdAndUpdate(req.params.itemId, {
            nome, 
            descricao, 
            valor, 
            valorAnterior, 
            imagemURI,
            usuario: req.userId
        }, { new:true });

        await item.save();

        return res.send({ item });
    } catch (err) {
        return res.status(400).send({ error: 'Erro ao criar um item' });
    }
});

router.delete('/:itemId', async (req, res) => {
    try {
        await Item.findByIdAndRemove(req.params.itemId);

        return res.send();
    } catch (err) {
        return res.status(400).send({ error: 'Erro ao remover item' });
    }
});

module.exports = app => app.use('/items', router);