const express = require('express');
const authMiddleware = require('../middlewares/auth');
const Post = require("./../models/post");
const multerConfig = require("./../../config/multer");
const multer = require("multer");
const router = express.Router();

router.use(authMiddleware);
  
router.post("/", multer(multerConfig).single("file"), async (req, res) => {
  const { originalname: name, size, key, location: url = "" } = req.file;

  const post = await Post.create({
    name,
    size,
    key,
    url
  });

  return res.json(post);
});

router.get("/", async (req, res) => {
  try {
    const posts = await Post.find();

    return res.json(posts);
  } catch (err) {
      return res.status(400).send({ error: 'Erro ao carregar lista de imagens' });
  }
});

router.get("/:postId", async (req, res) => {
  try {
    const post = await Post.findById(req.params.postId);

    return res.send({post});
  } catch (err) {
      return res.status(400).send({ error: 'Erro ao carregar imagem' });
  }
});

router.delete("/:postId", async (req, res) => {
  const post = await Post.findById(req.params.postId);

  await post.remove();

  return res.send();
});

module.exports = app => app.use('/posts', router);