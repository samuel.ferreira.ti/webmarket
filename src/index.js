const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const path = require('path');
require('dotenv/config');

const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(morgan('dev'));
app.use(
    "/files",
    express.static(path.resolve(__dirname, "..", "tmp", "uploads"))
  );

var chatUsers = {};

io.on('connection', socket => {
    chatUsers[socket.id] = socket;
    console.log(`Socket conectado: ${socket.id}`);
    socket.on('mensagem', data => {
        console.log(data);
    });
    socket.on('disconnect', reason => {
        console.log(`Socket desconectado: ${socket.id}`);
        delete chatUsers[socket.id];
    });
});

require('./app/controllers/index')(app);

app.get('/', async (req, res) => {
    res.send("API " + process.env.APP_NAME);
});

server.listen(process.env.PORT);
